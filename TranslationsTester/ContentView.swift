//
//  ContentView.swift
//  TranslationsTester
//
//  Created by Hannes Sverrisson on 25.2.2023.
//

import SwiftUI

struct ContentView: View {
    @State var showSheet = false
    
    let languageCodes = LangCode.allCases
    @State private var path: [LangCode] = []
    
    private let translations = TranslationService.shared
    
    var body: some View {
        if #available(iOS 16.0, *) {
            NavigationStack(path: $path) {
                List {
                    NavigationLink(value: languageCodes.first!) {
                        Label("German", systemImage: "globe")
                    }
                    NavigationLink(value: languageCodes[1]) {
                        Label("English", systemImage: "globe")
                    }
                    NavigationLink(value: languageCodes[2]) {
                        Label("French", systemImage: "globe")
                    }
                    NavigationLink(value: languageCodes.last!) {
                        Label("Italian", systemImage: "globe")
                    }
                }
                .navigationTitle("TranslationsTester")
                .navigationDestination(for: LangCode.self) { code in
                    TestView(code: code)
                }
            }
        } else {
            // Fallback on earlier versions
            NavigationView {
                List {
                    NavigationLink(destination: TestView(code: languageCodes.first!)) {
                        Label("German", systemImage: "globe")
                    }
                    NavigationLink(destination: TestView(code: languageCodes[1])) {
                        Label("English", systemImage: "globe")
                    }
                    NavigationLink(destination: TestView(code: languageCodes[2])) {
                        Label("French", systemImage: "globe")
                    }
                    NavigationLink(destination: TestView(code: languageCodes.last!)) {
                        Label("Italian", systemImage: "globe")
                    }
                }
                .navigationTitle("TranslationsTester")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
