//
//  TranslationsTesterApp.swift
//  TranslationsTester
//
//  Created by Hannes Sverrisson on 25.2.2023.
//

import SwiftUI
import os.log

@main
struct TranslationsTesterApp: App {
    @Environment(\.scenePhase) var scenePhase
    
    private let log = os.Logger(subsystem: "TranslationsTester", category: "TranslationsTesterApp")
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .onChange(of: scenePhase) { newPhase in
            log.info("Scene changed to: \(newPhase)")
            if newPhase == .active {
                NotificationCenter.default.post(name: .fetchTranslations, object: nil)
            }
        }
    }
}

extension ScenePhase: CustomStringConvertible {
    public var description: String {
        switch self {
        case .active:
            return "Active"
        case .inactive:
            return "InActive"
        case .background:
            return "Background"
        @unknown default:
            return "Unknown"
        }
    }
}
