//
//  TranslationService.swift
//  Bench
//
//  Created by Hannes Sverrisson on 20.2.2023.
//  Copyright © 2023 Headbits. All rights reserved.
//

import Foundation
import UIKit
import os.log

typealias TranslationDict = [String: String]

/// All the translations constants
enum TranslationConstant {
    static let bundleDict = Bundle.main.infoDictionary ?? [:]
    static let appVersion = bundleDict["CFBundleShortVersionString"] as? String ?? "1.0"
    static let platform = "ios"
    static let ApiVersion = "v1"
    static let domain = "https://hreim.headbits.agency"
    static let projectId = "ae5c460c-e788-4795-b5cd-4bdbf2420243"
    static let format = "simple_json"
    static let parameters = "appVersion=\(appVersion)&platform=\(platform)&projectId=\(projectId)&language="
    static let subsystem = "Bench"
    static let category = "T11Service"
    static let queue = "TranslationService"
}

/// All the codes for the supported languages, i.e. "de"
enum LangCode: String, CustomStringConvertible, CaseIterable {
    case de, en, fr, it
    
    var description: String {
        rawValue
    }
    
    /// Use translationPath to get the path for each language
    /// i.e.: https://hreim.headbits.agency/api/v1/ota?appVersion=1.0&platform=ios&projectId=ae5c460c-e788-4795-b5cd-4bdbf2420243&language=de
    var translationPath: URL? {
        let pathString = "\(TranslationConstant.domain)/api/\(TranslationConstant.ApiVersion)/ota?\(TranslationConstant.parameters)\(rawValue)"
        if let transPath = URL(string:pathString) {
            return transPath
        } else {
            assertionFailure("Translationpath broke: \(pathString)")
            return nil
        }
    }
}

extension Notification.Name {
    /// translationsUpdated is called when translation have been renewed and strings should be regenerated, always called on the main thread
    static let translationsUpdated = Notification.Name("translationsUpdated")
    
    /// fetchTranslations called to ask the service to check for updates
    static let fetchTranslations = Notification.Name("fetchTranslations")
}

extension String {
    func matches(usingRegex pattern: String) -> [String] {
        guard let regex = try? NSRegularExpression(pattern: pattern) else {
            assertionFailure("Regular expression failed")
            return []
        }
        let matches = regex.matches(in: self, range: NSRange(startIndex..., in: self))
        let substrings = matches.compactMap{ substring(with: $0.range) }
        return substrings.map { String($0) }
    }
    
    func substring(with nsrange: NSRange) -> Substring? {
        guard let range = Range(nsrange, in: self) else { return nil }
        return self[range]
    }
}

extension Locale {
    func langCode() -> LangCode? {
        guard let code = languageCode else { return nil }
        return LangCode(rawValue: code)
    }
}

/// A Singleton class to handle the current translations, by storing them in local memory and get updates over the internet.
/// It is thread-safe and updates it self on start or when app is moving to foreground
class TranslationService {
    static var shared = TranslationService()
    
    private let log = os.Logger(subsystem: TranslationConstant.subsystem, category: TranslationConstant.category)
    private let concurrentQueue = DispatchQueue(label: TranslationConstant.queue, attributes: .concurrent)
    private var isFetching = false
    
    
    init() {
        // Initialize
        DispatchQueue.global(qos: .userInitiated).async {
            let code = self.allowedCodes(Locale.current.identifier)
            self.loadTranslations(code: code)
        }
        log.info("Init")
#if SWIFTUI
        // Listen to when to check for updating translations
        NotificationCenter.default.addObserver(forName: .fetchTranslations, object: nil, queue: .current) { _ in
            self.log.info("Updating Translations, SwiftUI")
            self.fetchTranslations(code: self.localization ?? .de)
        }
#else
        // For a UIKit app, update when entering foreground
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .current) { _ in
            self.log.info("Updating Translations, UIKit")
            self.fetchTranslations(code: self.localization ?? .de)
        }
#endif
    }
    
    // MARK: - Thread safety
    
    private var _translations: TranslationDict = [:]
    public private(set) var translations: TranslationDict {
        get {
            concurrentQueue.sync {
                self._translations
            }
        }
        set(newVal) {
            concurrentQueue.async {
                self._translations = newVal
                self.log.info("Translations updated")
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .translationsUpdated, object: nil)
                }
            }
        }
    }
    
    private var _localization: LangCode?
    
    /// Set or get the localization code for the current user.
    /// Note: When set it will call the server for updates for the new language code
    public var localization: LangCode? {
        get {
            concurrentQueue.sync {
                return self._localization
            }
        }
        
        set(newVal) {
            guard let newVal, _localization != newVal else { return }
            concurrentQueue.async {
                self._localization = newVal
                self.loadTranslations(code: newVal)
                self.fetchTranslations(code: newVal)
            }
            log.info("Localization set to: \(newVal)")
        }
    }
    
    // MARK: - Public Methods
    
    /// Returns an updated translations string for the key provided, if the key is
    /// not updated it returns the current translation in the app.
    /// - Parameter key: Key to identify the translations string
    /// - Returns: Translated string to show to the user
    public func string(_ key: String) -> String {
        guard !key.isEmpty else {
            assertionFailure("An empty key provided for string translations!")
            return key
        }
        var translation = NSLocalizedString(key, comment: "")
        if let updatedTrans = translations[key] {
            translation = updatedTrans
        }
#if targetEnvironment(simulator)
        dispatchPrecondition(condition: .onQueue(.main))
#endif
        return translation
    }
    
    /// Returns an updated translations format string for the key provided and the variable arguments,
    /// if the key is not updated it returns the current translation in the app.
    /// - Parameters:
    ///   - key: key to identify the translations string
    ///   - arguments: the arguments for the format string
    /// - Returns: Translated string to show to the user
    public func string(key: String, arguments: CVarArg...) -> String {
        guard !key.isEmpty else {
            assertionFailure("An empty key provided for string translations!")
            return key
        }
        var translation = NSLocalizedString(key, comment: "")
        if let updatedTrans = translations[key] {
            translation = updatedTrans
        }
        // Check the arguments
        let formatCount = translation.matches(usingRegex: "%([0-9]\\$)*@").count
        if formatCount > arguments.count {
            let placeholder = "@"
            let args = arguments + Array(repeating: placeholder, count: formatCount - arguments.count)
            translation = String(format: translation, arguments: args)
            log.error("Format string for key `\(key)` requires more arguments (\(formatCount)) then provided (\(arguments.count)). \(placeholder) used instead of missing arguments")
        } else if formatCount < arguments.count {
            let args = Array(arguments[0..<formatCount])
            translation = String(format: translation, arguments: args)
            log.error("Format string for key `\(key)` requires less arguments (\(formatCount)) then provided (\(arguments.count)). Ignored redundant arguments")
        } else {
            translation = String(format: translation, arguments: arguments)
        }
#if targetEnvironment(simulator)
        dispatchPrecondition(condition: .onQueue(.main))
#endif
        return translation
    }

    /// Check if given user language code is available in translations,
    /// otherwise return the default German translations string code
    /// - Parameter code: String for the given users language code to check
    /// - Returns: String code for the current user language, if it is available in
    /// translations, otherwise return the default German translations string code
    public func allowedCodes(_ code: String) -> LangCode {
        var languageCode = LangCode.de
        if #available(iOS 16, *) {
            if let identifier = Locale.current.language.languageCode?.identifier {
                languageCode = LangCode(rawValue: identifier) ?? .de
            }
        } else {
            if let identifier = Locale.current.languageCode {
                languageCode = LangCode(rawValue: identifier) ?? .de
            }
        }
        return languageCode
    }
    
    // MARK: - Helper Methods
    
    // Load translations from the temp file or the embedded if no temp file
    private func loadTranslations(code: LangCode) {
        guard let fileURL = getTempDirectory(code) else { return }
        var data = try? Data(contentsOf: fileURL)
        if data == nil {
            // Fetch from bundle of no data in temp folder
            self.log.info("Fetch translations from bundle")
            if let bundleURL = Bundle.main.url(forResource: "\(code)", withExtension: "json") {
                data = try? Data(contentsOf: bundleURL)
            } else {
                self.log.error("Translations should be embedded with the app!")
                assertionFailure("Translations should be embedded with the app!")
            }
        }
        guard let data else { return }
        guard let transDict = try? JSONDecoder().decode(TranslationDict.self, from: data) else {
            assertionFailure("Could not decode json")
            self.log.error("Could not decode json")
            return
        }
        self.translations = transDict
    }
    
    // Convert the translations data
    private func convertData(_ data: Data) {
        let decoder = JSONDecoder()
        decoder.assumesTopLevelDictionary = true
        do {
            let dict = try decoder.decode(TranslationDict.self, from: data)
            log.info("Got \(dict.count) updated translations")
            translations = dict
        } catch {
            log.error("Converting data to translations: \(error.localizedDescription)")
            return
        }
    }
    
    // Fetch the translations from the server
    private func fetchTranslations(code: LangCode) {
        guard isFetching == false else { return }
        self.isFetching = true
        
        // Fetch updated translations
        guard let url = code.translationPath else {
            assertionFailure("Didn't get url for translations")
            return
        }
        self.log.info("Fetching Translations for \(code) from: \(url)")
        var request = URLRequest(url: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        let start = CFAbsoluteTimeGetCurrent()
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data else {
                assertionFailure("Did not get a valid data from translations server for url: \(url)")
                self.log.error("Did not get a valid data from translations server for url: \(url)")
                return
            }
            let time = (CFAbsoluteTimeGetCurrent() - start) * 1000
            self.log.info("Received OTA translations in: \(time) ms")
            self.convertData(data)
            self.saveToDisk(code: code)
            self.isFetching = false
        }.resume()
    }
    
    private func getTempDirectory(_ code: LangCode) -> URL? {
        let fileName = "\(code.rawValue)"
        guard let dir = try? FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {
            assertionFailure("Could not create a tempary cache directory")
            self.log.error("Could not create a tempary cache directory")
            return nil
        }
        return dir.appendingPathComponent(fileName, conformingTo: .json)
    }
    
    // Save translations in the temporary app folder
    private func saveToDisk(code: LangCode) {
        guard let fileURL = getTempDirectory(code) else { return }
        guard let data = try? JSONEncoder().encode(self.translations) else {
            assertionFailure("Could not encode to json")
            self.log.error("Could not encode to json")
            return
        }
        do {
            try data.write(to: fileURL, options: [.atomicWrite])
            self.log.info("Translations saved to: \(fileURL)")
        } catch {
            assertionFailure("Temporary save data failure")
            self.log.error("Temporary save data failure")
        }
    }
}

