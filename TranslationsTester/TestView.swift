//
//  TestView.swift
//  TranslationsTester
//
//  Created by Hannes Sverrisson on 25.2.2023.
//

import SwiftUI

struct TestView: View {
    var code = LangCode.de
    @State private var totalTranslations = 0
    @State var maxKeyWidth: CGFloat = .zero
    private let translations = TranslationService.shared
    
    let keys = [
        "__akzent-internal-version-tag",
        "quiz.answer.true",
        "welcome.start.paragraph1",
        "welcome.accountButton",
        "welcome.partners",
        "accountOverview.empty.noPayments",
    ]
    
    let formatKeys = [
        "appRating.description",
        "card.foundationCase107.article2",
        "deposits.limitSurpassed.warning.description.pkYes",
        "authorization.title",
    ]
    
    var body: some View {
        List {
            ForEach(keys, id: \.self) { key in
                VStack(alignment: .leading) {
                    Text("\(key)")
                        .font(.caption)
                        .foregroundColor(.accentColor)
                    Spacer(minLength: 4)
                    Text("\(self.translations.string(key))")
                        .font(.body)
                }
                .padding(.bottom)
            }
            ForEach(formatKeys, id: \.self) { key in
                VStack(alignment: .leading) {
                    Text("\(key)")
                        .font(.caption)
                        .foregroundColor(.accentColor)
                    Spacer(minLength: 4)
                    Text("\(self.translations.string(key: key, arguments: "test1", "test2", "test3"))")
                        .font(.body)
                }
                .padding(.bottom)
            }
        }
        .navigationTitle(code.rawValue)
        .task {
            translations.localization = code
        }
        .onReceive(NotificationCenter.default.publisher(for: .translationsUpdated), perform: { _ in
            self.totalTranslations = translations.translations.count
        })
        
        if totalTranslations > 0 {
            Text("Updated \(totalTranslations) translations")
                        .font(.caption)
                        .padding(4)
        } else {
            Text("Didn't get translations!")
                        .font(.caption)
                        .padding(4)
        }
    }
}

struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        TestView()
    }
}
