//
//  TranslationsTesterTests.swift
//  TranslationsTesterTests
//
//  Created by Hannes Sverrisson on 25.2.2023.
//

import XCTest
@testable import TranslationsTester

final class TranslationsTesterTests: XCTestCase {
    let translationsService = TranslationService.shared

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTranslationDe() throws {
        translationsService.localization = .de
        let exp = expectation(description: "Test after 1 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 1)
        if result == XCTWaiter.Result.timedOut {
            let keys = translationsService.translations.keys
            print("Testing \(keys.count) translations!")
            for key in keys {
                let translation = translationsService.string(key)
                XCTAssertNotEqual(key, translation)
            }
        } else {
            XCTFail("Delay interrupted")
        }
    }
    
    func testTranslationEn() throws {
        translationsService.localization = .en
        let exp = expectation(description: "Test after 1 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 1)
        if result == XCTWaiter.Result.timedOut {
            let keys = translationsService.translations.keys
            print("Testing \(keys.count) translations!")
            for key in keys {
                let translation = translationsService.string(key)
                XCTAssertNotEqual(key, translation)
            }
        } else {
            XCTFail("Delay interrupted")
        }
    }
    
    func testTranslationFr() throws {
        translationsService.localization = .fr
        let exp = expectation(description: "Test after 1 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 1)
        if result == XCTWaiter.Result.timedOut {
            let keys = translationsService.translations.keys
            print("Testing \(keys.count) translations!")
            for key in keys {
                let translation = translationsService.string(key)
                XCTAssertNotEqual(key, translation)
            }
        } else {
            XCTFail("Delay interrupted")
        }
    }
    
    func testTranslationIt() throws {
        translationsService.localization = .it
        let exp = expectation(description: "Test after 1 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 1)
        if result == XCTWaiter.Result.timedOut {
            let keys = translationsService.translations.keys
            print("Testing \(keys.count) translations!")
            for key in keys {
                let translation = translationsService.string(key)
                XCTAssertNotEqual(key, translation)
            }
        } else {
            XCTFail("Delay interrupted")
        }
    }
    
    func testTranslationsWithFormatstrings() throws {
        let formatKeys = [
            "appRating.description",
            "card.foundationCase107.article2",
            "deposits.limitSurpassed.warning.description.pkYes",
            "authorization.title",
        ]
        let exp = expectation(description: "Test after 1 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 1)
        if result == XCTWaiter.Result.timedOut {
            print("Testing \(formatKeys.count) formatKeys translations!")
            for key in formatKeys {
                let translation = translationsService.string(key: key, arguments: "test1", "test2", "test3")
                XCTAssertNotEqual(key, translation)
                XCTAssertFalse(translation.contains("%"))
                if translation.contains("\\") {
                    print(translation)
                }
                XCTAssertFalse(translation.contains("\\"))
            }
        } else {
            XCTFail("Delay interrupted")
        }
    }

    func testPerformanceExample() throws {
        let exp = expectation(description: "Test after 1 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 1)
        if result == XCTWaiter.Result.timedOut {
            let keys = translationsService.translations.keys
            self.measure {
                for key in keys {
                    let translation = translationsService.string(key)
                    XCTAssertNotEqual(key, translation)
                }
            }
        } else {
            XCTFail("Delay interrupted")
        }
    }

}
